﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="StudentsForm.aspx.cs" Inherits="PresentationLayer.StudentsForm" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <br />
    <div class="row"> 
        <asp:Label ID="lblName" runat="server" Text="Name"></asp:Label>
        <asp:TextBox ID="txtName" runat="server"></asp:TextBox>
    </div>
    <br />
    <div class="row"> 
        <asp:Label ID="lblSurname" runat="server" Text="Surname"></asp:Label>
        <asp:TextBox ID="txtSurname" runat="server"></asp:TextBox>
    </div>
    <div class="row">
        Budget?
        <asp:CheckBox ID="CheckBox" runat="server" />
    </div>
    <div class="row">
        <asp:Button ID="button" runat="server" Text="Insert" OnClick="button_Click" />
    </div>

    <div class="container">

        <asp:ListBox ID="listBox" runat="server"></asp:ListBox>

    </div>
</asp:Content>
