﻿using BusinessLayer;
using DataLayer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PresentationLayer
{
    public partial class StudentsForm : System.Web.UI.Page
    {

        StudentBusiness studentBusiness = new StudentBusiness();
        protected void Page_Load(object sender, EventArgs e)
        {
            FillStudents();
        }

        protected void button_Click(object sender, EventArgs e)
        {
            Student s = new Student();

            s.Name = txtName.Text;
            s.Surname = txtSurname.Text;
            s.IsBudget = CheckBox.Checked;

            studentBusiness.InsertStudent(s);
        }

        public void FillStudents()
        {
            List<Student> students = studentBusiness.GetBudgetStudents();

            foreach(Student s in students)
            {
                listBox.Items.Add(s.Name + " " + s.Surname);
            }
        }
    }
}