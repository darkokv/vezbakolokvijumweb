﻿using DataLayer;
using DataLayer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer
{
    public class StudentBusiness
    {
        private StudentRepository studentRepository;

        public StudentBusiness()
        {
            studentRepository = new StudentRepository();
        }

        public List<Student> GetStudents()
        {
            return studentRepository.GetAllStudents();
        }

        public int InsertStudent(Student s)
        {
            return studentRepository.InsertStudent(s);
        }

        public List<Student> GetBudgetStudents()
        {
            return studentRepository.GetAllStudents().Where(s => s.IsBudget == true).ToList();
        }

    }

}
