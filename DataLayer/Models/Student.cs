﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer.Models
{
    public class Student : Person
    {
        private bool isBudget;

        public bool IsBudget { get => isBudget; set => isBudget = value; }
    }
}
